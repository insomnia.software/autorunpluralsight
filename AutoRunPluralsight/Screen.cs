﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoRunPluralsight
{
	class Screen
	{
		public Color orange = Color.FromArgb(255, 249, 104, 22);
		public Color gey = Color.FromArgb(255, 211, 211, 211);
		private const int MOUSEEVENTF_LEFTDOWN = 0x02;
		private const int MOUSEEVENTF_LEFTUP = 0x04;
		private const int MOUSEEVENTF_RIGHTDOWN = 0x08;
		private const int MOUSEEVENTF_RIGHTUP = 0x10;

		public bool CaptureApplication(string procName)
		{
			var proc = Process.GetProcessesByName(procName)[0];
			var rect = new User32.Rect();
			User32.GetWindowRect(proc.MainWindowHandle, ref rect);

			int width = rect.right - rect.left;
			int height = rect.bottom - rect.top;

			var bmp = new Bitmap(width, height, PixelFormat.Format32bppArgb);
			Graphics graphics = Graphics.FromImage(bmp);
			graphics.CopyFromScreen(rect.left, rect.top, 0, 0, new Size(width, height), CopyPixelOperation.SourceCopy);
			//bmp.Save("c:\\home\\test.png", ImageFormat.Png);

			try
			{
				if (bmp.GetPixel(860, 575) == orange) //opt stop
				{
					StopAndStart();
					//go to orange box, click
					Click(860 + 2560, 575);
				}
				else if (bmp.GetPixel(875, 590) == orange)
				{
					StopAndStart();
					//go to orange box, click
					Click(875 + 2560, 590);
				}
			}
			catch (Exception)
			{
			}
			return true;
		}

		private void StopAndStart()
		{
			Console.WriteLine("Video has finished, restarting");
			//tab to obs
			Console.WriteLine("Tab to obs");
			TabToWindow(Process.GetProcessesByName("OBS")[0]);
			Thread.Sleep(2000);
			Click(1527, 1472);
			//SendKeys.SendWait("{F7}");
			//click stat
			Thread.Sleep(2000);
			Click(1527, 1472);
			//SendKeys.SendWait("{F9}");
			Thread.Sleep(2000);
			//tab to firefox
			Console.WriteLine("Tab to firefox");
			TabToWindow(Process.GetProcessesByName("firefox")[0]);
			Thread.Sleep(1000);
		}

		public void Click(int x, int y)
		{
			Console.WriteLine($"Clicking X:{x}, Y:{y}");
			MouseOperations.SetCursorPosition(x, y);
			MouseOperations.MouseEvent(MouseOperations.MouseEventFlags.LeftDown);
			Thread.Sleep(10);
			MouseOperations.MouseEvent(MouseOperations.MouseEventFlags.LeftUp);
		}

		private static bool TabToWindow(Process processName)
		{
			User32.SetForegroundWindow(processName.MainWindowHandle);
			return true;
		}

		private class User32
		{
			[StructLayout(LayoutKind.Sequential)]
			public struct Rect
			{
				public int left;
				public int top;
				public int right;
				public int bottom;
			}

			[DllImport("user32.dll")]
			public static extern IntPtr GetWindowRect(IntPtr hWnd, ref Rect rect);

			[DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
			public static extern void mouse_event(uint dwFlags, uint dx, uint dy, uint cButtons, uint dwExtraInfo);

			[DllImport("user32.dll")]
			public static extern bool SetForegroundWindow(IntPtr hWnd);
		}
	}

	public static class MouseOperations
	{
		[Flags]
		public enum MouseEventFlags
		{
			LeftDown = 0x00000002,
			LeftUp = 0x00000004,
			MiddleDown = 0x00000020,
			MiddleUp = 0x00000040,
			Move = 0x00000001,
			Absolute = 0x00008000,
			RightDown = 0x00000008,
			RightUp = 0x00000010
		}

		[DllImport("user32.dll", EntryPoint = "SetCursorPos")]
		[return: MarshalAs(UnmanagedType.Bool)]
		private static extern bool SetCursorPos(int X, int Y);

		[DllImport("user32.dll")]
		[return: MarshalAs(UnmanagedType.Bool)]
		private static extern bool GetCursorPos(out MousePoint lpMousePoint);

		[DllImport("user32.dll")]
		private static extern void mouse_event(int dwFlags, int dx, int dy, int dwData, int dwExtraInfo);

		public static void SetCursorPosition(int X, int Y)
		{
			SetCursorPos(X, Y);
		}

		public static void SetCursorPosition(MousePoint point)
		{
			SetCursorPos(point.X, point.Y);
		}

		public static MousePoint GetCursorPosition()
		{
			MousePoint currentMousePoint;
			var gotPoint = GetCursorPos(out currentMousePoint);
			if (!gotPoint) { currentMousePoint = new MousePoint(0, 0); }
			return currentMousePoint;
		}

		public static void MouseEvent(MouseEventFlags value)
		{
			MousePoint position = GetCursorPosition();

			mouse_event
				((int)value,
				 position.X,
				 position.Y,
				 0,
				 0)
				;
		}

		[StructLayout(LayoutKind.Sequential)]
		public struct MousePoint
		{
			public int X;
			public int Y;

			public MousePoint(int x, int y)
			{
				X = x;
				Y = y;
			}

		}

	}
}
